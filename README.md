# wine-quality

# Análisis de los componentes que pueden influir en la calidad del vino

# Autor
Jesús Vallejo Júlvez

# Acerca de este repositorio
Se encuadra en la segunda práctica para la asignatura "Tipología y ciclo de vida de los datos":
- Máster en Ciencia de Datos.
- Universitat Oberta de Catalunya.
- Consultora de la asignatura: Laia Subirats Maté.
